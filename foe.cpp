///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file foe.cpp
/// @version 1.0
///
/// @author Baishen Wang <baishen@hawaii.edu>
/// @date 15_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include "foe.h"

double fromFoeToJoule( double foe ) {
   return foe / FOE_IN_A_JOULE ;
}

double fromJouleToFoe( double joule ) {
   return joule * FOE_IN_A_JOULE ;
}

