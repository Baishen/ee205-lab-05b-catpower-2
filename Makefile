###############################################################################
### University of Hawaii, College of Engineering
### @brief Lab 05b - catPower 2 - EE 205 - Spr 2022
###
### @file Makefile
### @version 1.0 - Initial version
###
### Build and test an energy unit conversion program
###
### @author Baishen Wang <baishen@hawaii.edu>
### @date 15_Feb_2022
###
### @see https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC = g++
CFLAGS = -g -Wall -Wextra
TARGET = catPower
all: $(TARGET)
ev.o: ev.cpp ev.h
		$(CC) $(CFLAGS) -c ev.cpp
megaton.o: megaton.cpp megaton.h
		$(CC) $(CFLAGS) -c megaton.cpp
gge.o: gge.cpp gge.h
		$(CC) $(CFLAGS) -c gge.cpp
foe.o: foe.cpp foe.h
		$(CC) $(CFLAGS) -c foe.cpp
cat.o: cat.cpp cat.h
		$(CC) $(CFLAGS) -c cat.cpp
catPower.o: catPower.cpp ev.h megaton.h gge.h cat.h joule.h
		$(CC) $(CFLAGS) -c catPower.cpp
joule.o: joule.cpp joule.h
		$(CC) $(CFLAGS) -c joule.cpp
catPower: catPower.o ev.o megaton.o gge.o foe.o cat.o joule.o
		$(CC) $(CFLAGS) -o $(TARGET) catPower.o ev.o megaton.o gge.o foe.o cat.o joule.o
test: catPower 
		@./catPower 3.14 j j | grep -q "3.14 j is 3.14 j"
		@./catPower 3.14 j e | grep -q "3.14 j is 1.95983E+19 e"
		@./catPower 3.14 j m | grep -q "3.14 j is 7.50478E-16 m"
		@./catPower 3.14 j g | grep -q "3.14 j is 2.58862E-08 g"
		@./catPower 3.14 j f | grep -q "3.14 j is 3.14E-24 f"
		@./catPower 3.14 j c | grep -q "3.14 j is 0 c"
		@./catPower 3.14 j x |& grep -q "Unknown toUnit"
		@./catPower 3.14 m j | grep -q "3.14 m is 1.31378E+16 j"
		@./catPower 3.14 g e | grep -q "3.14 g is 2.37728E+27 e"
		@./catPower 3.14 f m | grep -q "3.14 f is 7.50478E+08 m"
		@./catPower 3.14 c g | grep -q "3.14 c is 0 g"
		@./catPower 3.14 x f | grep -q "Unknown fromUnit"
		@./catPower 3.14 c g e | grep -1 "Usage:"
		@echo "All tests pass"

clean:
		rm -f $(TARGET) *.o

