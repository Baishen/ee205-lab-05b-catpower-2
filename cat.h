///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file cat.h
/// @version 1.0
///
/// @author Baishen Wang <baishen@hawaii.edu>
/// @date 15_Feb_2022
///////////////////////////////////////////////////////////////////////////////
#pragma once
const double CAT_POWER = 0;
const char CATPOWER = 'c';

extern double fromCatPowerToJoule( double catPower );

extern double fromJouleToCatPower(double joule );

