///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file cat.cpp
/// @version 1.0
///
/// @author Baishen Wang <baishen@hawaii.edu>
/// @date 15_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include "cat.h"

double fromCatPowerToJoule( double catPower ) {
   return 0.0;
}

double fromJouleToCatPower( double joule ) {
   return 0.0;
}

