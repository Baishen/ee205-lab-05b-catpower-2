///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 03d - CatPower - EE 205 - Spr 2022
///
/// Usage:  catPower fromValue fromUnit toUnit
///    fromValue: A number that we want to convert
///    fromUnit:  The energy unit fromValue is in
///    toUnit:  The energy unit to convert to
///
/// Result:
///   Print out the energy unit conversion
///
/// Example:
///   $ ./catPower 3.45e20 e j
///   3.45E+20 e is 55.2751 j
///
/// Compilation:
///   $ g++ -o catPower catPower.cpp
///
/// @file catPower.cpp
/// @version 1.1
///
/// @see https://en.wikipedia.org/wiki/Units_of_energy
/// @see https://en.wikipedia.org/wiki/List_of_unusual_units_of_measurement#Energy
///
/// @author Baishen Wang <baishen@hawaii.edu>
/// @date   1/2/22
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include "ev.h"
#include "megaton.h"
#include "gge.h"
#include "foe.h"
#include "cat.h"
#include "joule.h"
#define DEBUG
// A Joule represents the amount of electricity required to run a 1 W device for 1 s

void helpUsage(void)
{
   printf( "Energy converter\n" );

   printf( "Usage:  catPower fromValue fromUnit toUnit\n" );
   printf( "   fromValue: A number that we want to convert\n" );
   printf( "   fromUnit:  The energy unit fromValue is in\n" );
   printf( "   toUnit:  The energy unit to convert to\n" );
   printf( "\n" );
   printf( "This program converts energy from one energy unit to another.\n" );
   printf( "The units it can convert are: \n" );
   printf( "   j = Joule\n" );
   printf( "   e = eV = electronVolt\n" );
   printf( "   m = MT = megaton of TNT\n" );
   printf( "   g = GGE = gasoline gallon equivalent\n" );
   printf( "   f = foe = the amount of energy produced by a supernova\n" );
   printf( "   c = catPower = like horsePower, but for cats\n" );
   printf( "\n" );
   printf( "To convert from one energy unit to another, enter a number \n" );
   printf( "it's unit and then a unit to convert it to.  For example, to\n" );
   printf( "convert 100 Joules to GGE, you'd enter:  $ catPower 100 j g\n" );
   printf( "\n" );
}

//extern double ELECTRON_VOLTS_IN_A_JOULE; // =6.24150974e18;
//extern double MEGATONS_IN_A_JOULE; // = 1/(4.184e15);
//extern double GASOLINE_GALLON_IN_A_JOULE; // = 1/1.213e8;
//extern double FOE_IN_A_JOULE; // = 1/1e24;
//extern double CAT_POWER; // = 0;
//
//
//extern char JOULE         ; //= 'j';
//extern char ELECTRON_VOLT ; //= 'e';
//extern char MEGATON ; //= 'm';
//extern char GASOLINE_GALLON ; //= 'g';
//extern char FOE ; //= 'f';
//extern char CATPOWER ; //= 'c';

//extern double fromElectronVoltsToJoule( double electronVolts ); // {
//   return electronVolts / ELECTRON_VOLTS_IN_A_JOULE ;
//}


//extern double fromJouleToElectronVolts( double joule ); //{
//   return joule * ELECTRON_VOLTS_IN_A_JOULE ;
//}


//extern double fromMegatonToJoule( double megaton ); //{
//   return megaton / MEGATONS_IN_A_JOULE ;  
//}

//extern double fromJouleToMegaton( double joule ); //{
//   return joule * MEGATONS_IN_A_JOULE ; 
//}

//extern double fromGasolineToJoule(double gasoline); //{
//   return gasoline / GASOLINE_GALLON_IN_A_JOULE ;
//}

//extern double fromJouleToGasoline( double joule); //{
//   return joule * GASOLINE_GALLON_IN_A_JOULE ; 
//}

//extern double fromFoeToJoule( double foe ); //{
//   return foe / FOE_IN_A_JOULE ;
//}

//extern double fromJouleToFoe( double joule ); //{
//   return joule * FOE_IN_A_JOULE ;
//}

//extern double fromCatPowerToJoule( double catPower ); //{
//   return 0.0;  
//}

//extern double fromJouleToCatPower( double joule ); //{
//   return 0.0;
//}



int main( int argc, char* argv[] ) {
   
   if (argc < 4)
   {
      helpUsage();
      printf("first");
   }

   if (argc > 4)
   {
      helpUsage();
      printf("second");
   }

   double fromValue;
   char   fromUnit;
   char   toUnit;

   fromValue = atof( argv[1] );
   fromUnit = argv[2][0];         // Get the first character from the second argument
   toUnit = argv[3][0];           // Get the first character from the thrid argument


   double commonValue;
   switch( fromUnit ) {
      case JOULE           : commonValue = fromValue; // No conversion necessary
                           break;
      case ELECTRON_VOLT   : commonValue = fromElectronVoltsToJoule( fromValue );
                           break;
      case MEGATON         : commonValue = fromMegatonToJoule ( fromValue );
                           break;
      case GASOLINE_GALLON: commonValue = fromGasolineToJoule( fromValue);
                           break;
      case FOE             : commonValue= fromFoeToJoule( fromValue );
                           break;
      case CATPOWER        : commonValue = fromCatPowerToJoule( fromValue );
                           break;
      default:
                           printf( "Unknown fromUnit [%c]\n", fromUnit );
                           printf("\n");
                           helpUsage();
                           exit( EXIT_FAILURE );
   }
   #ifdef DEBUG
      printf("fromValue = [%lG]\n", fromValue );
   #endif

   #ifdef DEBUG
      printf( "fromUnit = [%c]\n", fromUnit );
   #endif

   #ifdef DEBUG 
      printf( "toUnit = [%c]\n", toUnit );
   #endif

   #ifdef DEBUG
      printf( "commonValue = [%lG] joule\n", commonValue );
   #endif

 
	///       Don't forget to break; after every case.
   double toValue;
   switch( toUnit ) {
      case JOULE           : toValue = commonValue;
                           break;
      case ELECTRON_VOLT   : toValue = fromJouleToElectronVolts( commonValue );
                           break;
      case MEGATON         : toValue = fromJouleToMegaton( commonValue );
                           break;
      case GASOLINE_GALLON : toValue = fromJouleToGasoline( commonValue );
                           break;
      case FOE             : toValue = fromJouleToFoe( commonValue);
                           break;  
      case CATPOWER        : toValue = fromJouleToCatPower( commonValue );
                           break;
      default:
                           printf( "Unknown toUnit [%c]\n", fromUnit );  
                           printf("\n");
                           helpUsage();
                           exit( EXIT_FAILURE );

   }
   printf( "%lG %c is %lG %c\n", fromValue, fromUnit, toValue, toUnit );
   return ( EXIT_SUCCESS );

}

