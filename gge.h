///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file gge.h
/// @version 1.0
///
/// @author Baishen Wang <baishen@hawaii.edu>
/// @date 15_Feb_2022
///////////////////////////////////////////////////////////////////////////////
#pragma once
const double GASOLINE_GALLON_IN_A_JOULE = 1/1.213e8;
const char GASOLINE_GALLON = 'g';

extern double fromGasolineToJoule( double gasoline );
extern double fromJouleToGasoline( double joule );

